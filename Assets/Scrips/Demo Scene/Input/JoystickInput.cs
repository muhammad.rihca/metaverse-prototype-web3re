using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickInput : MonoBehaviour
{
    public GameObject leftJoyObject;
    public GameObject rightJoyObject;
    public VariableJoystick leftJoystick;
    public VariableJoystick rightJoystick;
    protected CharacterInputHandler control;

    protected float cameraAngel;
    public float cameraAngelSpeed = 5f;

    // Start is called before the first frame update
    void Start()
    {
        control = GetComponent<CharacterInputHandler>();

#if MOBILE_INPUT
            leftJoyObject.SetActive(true);
            rightJoyObject.SetActive(true);
#endif

    }

#if MOBILE_INPUT
    // Update is called once per frame
    void Update()
    {
        control.moveInputVector.x = leftJoystick.Horizontal;
        control.moveInputVector.y = leftJoystick.Vertical;

        cameraAngel += rightJoystick.Horizontal * cameraAngelSpeed;

        control.viewInputVector.x = rightJoystick.Horizontal;
        control.viewInputVector.y = rightJoystick.Vertical * -1;

        // Camera.main.transform.position = transform.position + Quaternion.AngleAxis(cameraAngel, Vector3.up) * new Vector3(0, 3, 4);
        // Camera.main.transform.rotation = Quaternion.LookRotation(transform.position + Vector3.up * 2f - Camera.main.transform.position, Vector3.up);
    }
#endif
}
