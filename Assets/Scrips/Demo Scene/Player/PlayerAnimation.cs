using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fusion;

public class PlayerAnimation : NetworkBehaviour
{
    public Animator animator;
    CharacterInputHandler characterInputHandler;

    // Start is called before the first frame update
    void Start()
    {
        characterInputHandler = GetComponent<CharacterInputHandler>();
    }

    public void SetTriggerAnim(string trigger)
    {
        animator.SetTrigger(trigger);
    }

    // Update is called once per frame
    public override void FixedUpdateNetwork()
    {
        if (!characterInputHandler)
        {
            return;
        }

        if (characterInputHandler.moveInputVector.x >= 1 || characterInputHandler.moveInputVector.y >= 1)
        {
            SetTriggerAnim("Run");
        }
        else if (characterInputHandler.moveInputVector.x != 0 || characterInputHandler.moveInputVector.y != 0)
        {
            SetTriggerAnim("Walk");
        }
        else
        {
            SetTriggerAnim("Idle");
        }
    }
}
