using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fusion;

public class NPCHandler : NetworkBehaviour
{
    public GameObject[] skin;
    public NPCAnimation nPCAnimation;
    [SerializeField]
    float nPCMoveSpeed;
    TickTimer timeDuration = TickTimer.None;
    NetworkObject networkObject;
    NetworkRigidbody networkRigidbody;

    GameObject anchorPointSpawn;


    // Start is called before the first frame update
    void Start()
    {
        nPCMoveSpeed = Random.Range(1f, 3f);
        if (nPCMoveSpeed <= 1.5f)
        {
            nPCAnimation.SetTriggerAnim("Walk");
        }
        else
        {
            nPCAnimation.SetTriggerAnim("Run");
        }

        int skinLength = skin.Length;
        int showSkin = Random.Range(0, skinLength);
        for (int i = 0; i < skinLength; i++)
        {
            if (i == showSkin)
            {
                skin[i].SetActive(true);
                Debug.Log("skin" + i);
            }
            else
            {
                skin[i].SetActive(false);
            }
        }

        Movement(new Vector3(nPCMoveSpeed, nPCMoveSpeed, nPCMoveSpeed));
        anchorPointSpawn = GameObject.Find($"SpawnPointNPCs/SpawnPointNPC ({Random.Range(0, 4)})/AnchorSpawnPoint");

        transform.position = anchorPointSpawn.transform.position;
        Vector3 rotation = new Vector3(0, Random.Range(0, 360), 0);
        transform.eulerAngles = rotation;
    }

    public void Movement(Vector3 moveVector)
    {
        networkObject = GetComponent<NetworkObject>();
        networkRigidbody = GetComponent<NetworkRigidbody>();

        networkRigidbody.Rigidbody.AddForce(moveVector, ForceMode.Impulse);

        timeDuration = TickTimer.CreateFromSeconds(Runner, 7);
    }

    public override void FixedUpdateNetwork()
    {
        transform.position += transform.forward * Runner.DeltaTime * nPCMoveSpeed;

        if (Object.HasInputAuthority)
        {
            Debug.Log("Author");
            if (timeDuration.Expired(Runner))
            {
                Runner.Despawn(networkObject);
                timeDuration = TickTimer.None;
            }
        }
    }

    public override void Despawned(NetworkRunner runner, bool hasState)
    {
        Debug.Log("Despawn NPC");
    }
}
