using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fusion;

public class NPCSpawner : NetworkBehaviour
{
    public NPCHandler nPCHandler;
    NetworkObject networkObject;

    int totalNPC = 0;

    GameObject spawnPointNPC;

    void Awake()
    {
        networkObject = GetComponent<NetworkObject>();
    }

    public override void FixedUpdateNetwork()
    {
        if (Object.HasInputAuthority)
        {
            if (GetInput(out NetworkInputData networkInputData))
            {
                if (networkInputData.isNPCSpawned)
                {
                    SpawnNewNPC();
                }
            }
        }
    }

    void SpawnNewNPC()
    {
        // spawnPointNPC = GameObject.Find("SpawnPointNPC");
        // spawnPointNPC = transform.FindChild("AnchorSpawnPoint");

        Runner.Spawn(nPCHandler);
        // Instantiate(nPCHandler, new Vector3(2, 2, 2), Quaternion.identity);
    }
}
