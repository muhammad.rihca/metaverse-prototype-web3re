using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fusion;

public class PlayerHandler : NetworkBehaviour
{
    [Networked(OnChanged = nameof(OnPlayerChanged))]
    public bool isSkinChanging { get; set; }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnChangedSkinRemote()
    {

    }
    static void OnPlayerChanged(Changed<PlayerHandler> changed)
    {

    }
}
