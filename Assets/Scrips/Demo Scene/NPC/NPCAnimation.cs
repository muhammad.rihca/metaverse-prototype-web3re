using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fusion;

public class NPCAnimation : NetworkBehaviour
{
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void SetTriggerAnim(string trigger)
    {
        animator.SetTrigger(trigger);
    }
}
