using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInputHandler : MonoBehaviour
{
    public Vector2 moveInputVector = Vector2.zero;
    public Vector2 viewInputVector = Vector2.zero;
    // public float rotationSpeed;

    // Other components
    CharacterMovementHandler characterMovementHandler;
    LocalCameraHandler localCameraHandler;

    bool isNPCSpawned = false;
    int totalNPCSpawned = 0;

    void Awake()
    {
        characterMovementHandler = GetComponent<CharacterMovementHandler>();
        localCameraHandler = GetComponentInChildren<LocalCameraHandler>();
    }

    // Start is called before the first frame update
    void Start()
    {
        // Cursor.lockState = CursorLockMode.Locked;
        // Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!characterMovementHandler.Object.HasInputAuthority)
        {
            return;
        }

#if !MOBILE_INPUT
        // View input
        viewInputVector.x = Input.GetAxis("Mouse X");
        viewInputVector.y = Input.GetAxis("Mouse Y"); // insert the mouse look
#endif

        // Move Input
        moveInputVector.x = Input.GetAxis("Horizontal");
        moveInputVector.y = Input.GetAxis("Vertical");

        // TPS
        // characterMovementHandler.SetViewInputVector(viewInputVector);

        // Set view
        localCameraHandler.SetViewInputVector(viewInputVector);

        if (totalNPCSpawned < 10)
        {
            isNPCSpawned = true;
            totalNPCSpawned -= -1;
        }
    }

    public NetworkInputData GetNetworkInput()
    {
        NetworkInputData networkInputData = new NetworkInputData();

        // TPS
        // networkInputData.rotationInput = viewInputVector.x;

        // FPS
        networkInputData.aimForwardVector = localCameraHandler.transform.forward;

        // Move data
        networkInputData.movementInput = moveInputVector;

        networkInputData.isNPCSpawned = isNPCSpawned;

        isNPCSpawned = false;

        return networkInputData;
    }
}
