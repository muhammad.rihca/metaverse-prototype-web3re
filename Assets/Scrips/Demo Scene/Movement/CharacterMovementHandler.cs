using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fusion;

public class CharacterMovementHandler : NetworkBehaviour
{
    Vector2 viewInput;

    // Rotation
    float cameraRotationX = 0;
    Camera localCamera;
    NetworkCharacterControllerPrototypeCustom netCustom;

    public float rotationSpeed;

    void Awake()
    {
        netCustom = GetComponent<NetworkCharacterControllerPrototypeCustom>();
        localCamera = GetComponentInChildren<Camera>();
    }

    public override void FixedUpdateNetwork()
    {
        if (GetInput(out NetworkInputData networkInputData))
        {
            // TPS
            // Roate the view
            // netCustom.Rotate(networkInputData.rotationInput);

            // FPS rotate
            transform.forward = networkInputData.aimForwardVector;
            // Cancel out rotation on X axis as we don't want our character to tilt
            Quaternion rotation = transform.rotation;
            rotation.eulerAngles = new Vector3(0, rotation.eulerAngles.y, rotation.eulerAngles.z);
            transform.rotation = rotation;

            // Move
            Vector3 moveDirection = transform.forward * networkInputData.movementInput.y + transform.right * networkInputData.movementInput.x;
            moveDirection.Normalize();

            netCustom.Move(moveDirection);
        }
    }
}
