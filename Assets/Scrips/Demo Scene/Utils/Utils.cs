using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils
{
    public static Vector3 GetRandomSpawnPoint()
    {
        return new Vector3(0, 1, -5);
    }

    public static void SetRenderLayerInChildren(Transform transform, int layerNumber)
    {
        foreach (Transform trans in transform.GetComponentsInChildren<Transform>(true))
        {
            // if (trans.CompareTag("IgnoreLayerChange"))
            // continue;

            trans.gameObject.layer = layerNumber;
        }
    }
}
