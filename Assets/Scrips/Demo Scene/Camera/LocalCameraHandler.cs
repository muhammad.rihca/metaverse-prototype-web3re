using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalCameraHandler : MonoBehaviour
{
    public Transform fPSCameraAnchorPoint;
    public Transform tPSCameraAnchorPoint;

    public Transform playerModel;
    public NetworkPlayer networkPlayer;

    bool isFPS = true;

    // Default Camera Position
    Vector3 anchorCamera;

    // Input
    Vector2 viewInput;

    // Rotation
    float cameraRotationX = 0;
    float cameraRotationY = 0;

    // Other Components
    NetworkCharacterControllerPrototypeCustom netCustom;
    Camera localCamera;

    void Awake()
    {
        localCamera = GetComponent<Camera>();
        netCustom = GetComponentInParent<NetworkCharacterControllerPrototypeCustom>();
    }

    // Start is called before the first frame update
    void Start()
    {
        // Detach camera if enabled
        if (localCamera.enabled)
        {
            localCamera.transform.parent = null;
        }
    }

    void LateUpdate()
    {
        if (fPSCameraAnchorPoint == null || tPSCameraAnchorPoint == null)
        {
            return;
        }

        if (!localCamera.enabled)
        {
            return;
        }

        // Move camera follow the player
        if (isFPS)
        {
            localCamera.transform.position = fPSCameraAnchorPoint.position;
        }
        else
        {
            localCamera.transform.position = tPSCameraAnchorPoint.position;
        }

        // Calculate rotation
        cameraRotationX += viewInput.y * Time.deltaTime * netCustom.viewUpDownRotationSpeed;
        cameraRotationX = Mathf.Clamp(cameraRotationX, -90, 90);

        cameraRotationY += viewInput.x * Time.deltaTime * netCustom.rotationSpeed;

        // Apply rotation
        localCamera.transform.rotation = Quaternion.Euler(cameraRotationX, cameraRotationY, 0);
    }

    public void SetViewInputVector(Vector2 viewInput)
    {
        this.viewInput = viewInput;
    }

    public void ChangeCamera()
    {
        if (isFPS)
        {
            if (networkPlayer.Object.HasInputAuthority)
            {
                isFPS = false;
                Debug.Log($"Change cam {isFPS} in true");
                Utils.SetRenderLayerInChildren(playerModel, LayerMask.NameToLayer("Default"));
            }
        }
        else
        {
            if (networkPlayer.Object.HasInputAuthority)
            {
                isFPS = true;
                Debug.Log($"Change cam {isFPS} in false");
                Utils.SetRenderLayerInChildren(playerModel, LayerMask.NameToLayer("LocalPlayerModel"));
            }
        }
    }
}
